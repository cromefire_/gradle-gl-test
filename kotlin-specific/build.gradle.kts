plugins {
    java
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    // Kotlin
    maven("https://gitlab.com/api/v4/projects/23743161/packages/maven")
}

dependencies {
    // Kotlin
    implementation("factory.mikrate:mikrate-dialects-api:0.1.0")
}
