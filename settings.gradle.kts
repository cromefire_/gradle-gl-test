rootProject.name = "gradle-gl-test"

include(":groovy-specific", ":groovy-unified", ":kotlin-specific", ":kotlin-unified")
